from datetime import date
person = {"first_name" :  "Mariah", "last_name" : "Carey" , "birth_date" : "27.03.1970 ","hobbies": ["Sing", "Compose", "Act"]}

digit = input("Enter digit between 1-7!: ")
if(digit == '1'):
    print(person["last_name"])
elif(digit == '2'):
    print("Month %s" % person["birth_date"].split(".")[1])
elif(digit == '3'):
    print(len(person["hobbies"]))
elif(digit == '4'):
    print((person["hobbies"][-1]))
elif(digit == '5'):
    person["hobbies"].append("Cooking")
elif(digit == '6'):
    print(tuple(person["birth_date"].split(".")))
elif(digit == '7'):
    birth_date = person["birth_date"].split(".")
    age = int(date.today().year) - int(birth_date[2])  - ((date.today().month , date.today().day) < (int(birth_date[1]) , int(birth_date[0])))
    person["age"] = age
    print("%s -> %s" % ("age" , person["age"]))



