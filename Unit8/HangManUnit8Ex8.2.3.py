def mult_tuple(tuple1, tuple2):
    '''
    this function gets 2 tuples and return a typle that contains all the
    possible tuples combinetions
    :param tuple1:
    :param tuple2:
    :return a tubple:
    '''
    tp = ()
    for i in tuple1:
        for y in tuple2:
            tp = (*tp , (i , y) , (y ,  i))
    return tp
first_tuple = (1, 2)
second_tuple = (4, 5)
print(mult_tuple(first_tuple, second_tuple))