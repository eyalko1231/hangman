def sort_anagrams(list_of_strings):
    '''
    this function gets a list of words and creates a list of list that have the words with the same letters
    :param list_of_strings:
    :return a list:
    '''
    groups = []
    for word in list_of_strings:
        found = False
        for group in groups:
            if sorted(word) == sorted(group[0]):
                group.append(word)
                found = True
                break
        if not found:
            groups.append([word])
    return groups

list_of_words = ['deltas', 'retainers', 'desalt', 'pants', 'slated', 'generating', 'ternaries', 'smelters', 'termless', 'salted', 'staled', 'greatening', 'lasted', 'resmelts']

print(sort_anagrams(list_of_words))