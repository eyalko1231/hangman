
def count_chars(my_str):
    '''
    this function get string and maps the letters and there amount
    :param string my_str:
    :return maped letter in string:
    ;:rtype dic
    '''
    dic = {}
    for letter in my_str:
        if letter != ' ':
            if letter not in dic:
                dic[letter] = 1
            else:
                dic[letter] += 1
    return dic


magic_str = "abra cadabra"
print(count_chars(magic_str))