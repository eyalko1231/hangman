#8.2.2
def sort_prices(list_of_tuples):
    '''
    this function gets a list of tuples and return a sorted list by the second varible
    in the tuple, it uses the get_key function.
    :param list_of_tuples:
    :return a sorted list of tuples:
    '''
    return sorted(list_of_tuples,key=get_key,reverse=True)

def get_key(price):
    '''
    function that return the second varible
    :param price:
    :return:
    '''
    return price[1]

products = [('milk', '5.5'), ('candy', '2.5'), ('bread', '9.0')]
print(sort_prices(products))