def my_mp3_playlist(file_path):
    '''
    this function read a file that represant songs and return the name of the
    longest song, the amount of song and the common singer
    :param file_path:
    :return tuple:
    '''
    file = open(file_path,"r")
    max = ("0:0","")
    max_singer = ("",0)
    common_singer = {}
    counter = 0
    for line in file:
        counter += 1
        time = [line.split(";")[2],line.split(";")[0]]
        if(int(time[0].split(":")[0]) > int(max[0].split(":")[0])):
            max = time
        if(line.split(";")[1] not in common_singer.keys()):
            common_singer[line.split(";")[1]] = 1;
        else:
            common_singer[line.split(";")[1]] += 1
    for k,v in common_singer.items():
        if(v > max_singer[1]):
            max_singer = (k , v)
    return (max[1],counter,max_singer[0])


print(my_mp3_playlist("work1.txt"))