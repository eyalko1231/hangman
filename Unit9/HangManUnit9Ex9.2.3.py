import math
def who_is_missing(file_name):
    '''
    gets a path of files that has 2,4,5,6 numbers that seprated by ,
    the function finds the missing number prints it and write it into
    foundfile.txt
    :param file_name:
    :return:
    '''
    file = open(file_name, "r")
    foundfile = open("foundfile.txt", "w")
    num_list = file.read().split(",")
    for num in range(1,int(max(num_list))):
        if str(num) not in num_list:
            print(num)
            foundfile.write(str(num))
    file.close()
    foundfile.close()

who_is_missing("work1.txt")