
def last_early(my_str):
    return my_str[-1:] in my_str[:-1]

print(last_early("best of luck"))