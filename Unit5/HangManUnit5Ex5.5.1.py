
def is_valid_input(letter_guessed):
    """

    :param letter_guessed:
    :return true if its good:
    """
    return (len(letter_guessed)) < 2 and str(letter_guessed).isalpha()
