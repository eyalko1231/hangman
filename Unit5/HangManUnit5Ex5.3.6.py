
def filter_teens(a = 13, b = 13 , c = 13):
    return fix_ages(a) + fix_ages(b) + fix_ages(c)

def fix_ages(age):
    if( age > 19 or age < 13 or 15 <= age <= 16):
        return age
    else:
        return 0

print(filter_teens(1,15,2))