def numbers_letters_count(my_str):
    '''
    this function gets a str and return a list that contains
    the number of letter in the str and the number of digits
    :param my_str:
    :return list:
    '''
    digit = 0
    letters = 0
    for item in my_str:
        if(str(item).isdigit()):
            digit += 1
        else:
            letters += 1
    return [digit,letters]