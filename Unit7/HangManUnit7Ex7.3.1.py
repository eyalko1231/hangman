def show_hidden_word(secret_word, old_letters_guessed):
    """
        this function gets a word and a list of letters
        and its return a string of the letters that in the list
        that belong to the word
        example:
        if the word is hello and the list is e,o,r
        it will return _ e _ _ o
        :param secret_word:
        :param old_letters_guessed:
        :return string:
        """
    hidden_word = []
    for letter in secret_word:
        if(letter in old_letters_guessed):
            hidden_word.append(letter)
        else:
            hidden_word.append("_")
    return " ".join(hidden_word)