def is_greater(my_list, n):
    '''
    this function gets list and a number and it returns
     new list which contains all the numbers who bigger the given number
    :param my_list:
    :param n:
    :return a list:
    '''
    list_big_n = list()
    for item in my_list:
        if(item > n):
            list_big_n.append(item)
    return list_big_n