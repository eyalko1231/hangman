def seven_boom(end_number):
    '''
    gets a number and return a list like seven boom
    conatins all the number and the numbers of that contains 7 or divide by
    :param end_number:
    :return: list
    '''
    seven_list = list()
    for num in range(end_number + 1):
        if(num / 7 == 0 or '7' in str(num)):
            seven_list.append("BOOM")
        else:
            seven_list.append(num)
    return seven_list