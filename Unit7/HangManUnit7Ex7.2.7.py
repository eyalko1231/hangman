def arrow(my_char, max_length):
    '''
    this function create arrow fron the given char and lemgth
    :param my_char:
    :param max_length:
    :return: a
    '''
    arrow = ""
    for i in range(max_length):
        arrow = arrow + my_char * (i+1) + '\n'
    for i in range(max_length,-1,-1):
        arrow = arrow + my_char * (i + 1) + '\n'
    return arrow