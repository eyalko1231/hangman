def stuiped_course():
    '''
    this function get an input by user
    and preforme the action by the digit
    :return:
    '''
    products = input("Please enter products: ")
    products_list = products.split(",")
    digit = 0
    while(digit != 9):
        digit = (int)(input("enter digit: "))
        if(digit == 1):
            print(",".join(products_list))
        elif(digit == 2):
            print(len(products_list))
        elif(digit == 3):
            check_if_exist(products_list)
        elif(digit == 4):
            prodcut_name = input("enter product name: ")
            print(list(products_list).count(prodcut_name))
        elif(digit == 5):
            prodcut_name = input("enter product name to remove: ")
            products_list.remove(prodcut_name)
        elif(digit == 6):
            prodcut_name = input("enter product name to add: ")
            products_list.append(prodcut_name)
        elif(digit == 7):
            check_valid(products_list)
        else:
            products_list = remove_dup(products_list)



def remove_dup(prodcut_list):
    '''
    this function duplicates form the list
    :param prodcut_list:
    :return: list
    '''
    temp_list = []
    for item in prodcut_list:
        if(item not in temp_list):
            temp_list.append(item)
    return temp_list

def check_if_exist(product_list):
    '''
    check if the product name exits in the given list
    :param product_list:
    :return:
    '''
    prodcut_name = input("enter product name: ")
    if (prodcut_name in product_list):
        print("yes")
    else:
        print("no")

def check_valid(product_list):
    '''
    this function gets list and remove not valid items
    :param product_list:
    :return:
    '''
    for item in product_list:
       if(len(item) < 3 or not str(item).isalpha()):
           print(item)
