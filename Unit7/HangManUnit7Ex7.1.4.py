def squared_numbers(start, stop):
    '''
    this function gets 2 numbers as range and a append all the square
    of the number
    :param start:
    :param stop:
    :return list:
    '''
    squred_list = list()
    while(start <= stop):
        squred_list.append(start * start)
        start = start + 1
    return squred_list