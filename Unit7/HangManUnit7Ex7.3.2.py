def check_win(secret_word, old_letters_guessed):
    """
       this function gets a word and a list of letter
       and checks if all the letters of the word is in
       the list.
       if yes return true , otherwise false
       :param secret_word:
       :param old_letters_guessed:
       :return boolean if the word in the letter list:
       """
    for letter in secret_word:
        if(letter not in old_letters_guessed):
            return False
    return True