
MAX_TRIES = 6

HANGMAN_PHOTOS = {
    0 : """
    x-------x
""",
    1: """
    x-------x
    |
    |
    |
    |
    |
""",
    2 : """
    x-------x
    |       |
    |       0
    |
    |
    |
""",
    3 : """
    x-------x
    |       |
    |       0
    |       |
    |
    |
""",
    4: """
    x-------x
    |       |
    |       0
    |      /|\\
    |
    |""",
    5: """
    x-------x
    |       |
    |       0
    |      /|\\
    |      /
    |""",
    6: """
    x-------x
    |       |
    |       0
    |      /|\\
    |      / \\
    |"""
}

def print_hangman(num_of_tries):
    '''
    this function print the current state of the hangman
    :param int num_of_tries:
    :return:
    '''
    print(HANGMAN_PHOTOS[num_of_tries])



def choose_word(file_path, index):
    '''
    this function gets filepath and index ,the return the word in that index
    and the amount of diffrent words in file
    :param file_path:
    :param index:
    :return:
    '''
    file = open(file_path,"r")

    my_word = ""
    counter = 1
    text = file.read().split(" ")
    isfound = True
    for word in text:
        counter = counter % len(text)
        if(counter == index and isfound):
            my_word = word
            isfound = False
        counter += 1
    return my_word



def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    '''
    this function gets a letter and the list of letter and checks if the given letter
    is in the list.
    if not append it into the list, otherwise print X and the letter in the list
    :param letter_guessed:
    :param old_letters_guessed:
    :return boolean if it added to the list:
    '''
    if(check_valid_input(letter_guessed,old_letters_guessed)):
        old_letters_guessed.append(letter_guessed)
        return True
    print("X")
    print("->".join(list(sorted(old_letters_guessed))))
    return False


def check_win(secret_word, old_letters_guessed):
    """
    this function gets a word and a list of letter
    and checks if all the letters of the word is in
    the list.
    if yes return true , otherwise false
    :param secret_word:
    :param old_letters_guessed:
    :return boolean if the word in the letter list:
    """
    for letter in secret_word:
        if(letter not in old_letters_guessed):
            return False
    return True

def check_valid_input(letter_guessed, old_letters_guessed):
    '''
    this function gets a letter and a letter list,
    and check if the letter given is valid and if it not in the
    letter list
    :param letter_guessed:
    :param old_letters_guessed:
    :return true if the letter is valid and false otherwise:
    '''
    return ((len(letter_guessed) < 2) and str(letter_guessed).isalpha()
            and str(letter_guessed).lower()
            not in old_letters_guessed)

def show_hidden_word(secret_word, old_letters_guessed):
    """
    this function gets a word and a list of letters
    and its return a string of the letters that in the list
    that belong to the word
    example:
    if the word is hello and the list is e,o,r
    it will return _ e _ _ o
    :param secret_word:
    :param old_letters_guessed:
    :return string:
    """
    hidden_word = []
    for letter in secret_word:
        if(letter in old_letters_guessed):
            hidden_word.append(letter)
        else:
            hidden_word.append("_")
    return " ".join(hidden_word)


def main():
    path_to_file = input("Enter path to file: ")
    index = int(input("Enter word index: "))
    secret_word = choose_word(path_to_file,index)
    num_of_tries = 0
    old_letters_guessed = []
    print("Let’s start!\n")

    #the loop that manage the program, it will loop until the game
    #is not over
    while(num_of_tries < 6 and not check_win(secret_word,old_letters_guessed)):
        print_hangman(num_of_tries)
        print(show_hidden_word(secret_word, old_letters_guessed))
        letter_guessed = input("Guess a letter: ")
        if(try_update_letter_guessed(letter_guessed, old_letters_guessed)):
            if(letter_guessed not in secret_word):
                #if we did wrong we increase the number of tries by one
                num_of_tries += 1
    if( num_of_tries == 6):
        print("LOSE")
    else:
        print("WIN")


if __name__ == '__main__':
    main()