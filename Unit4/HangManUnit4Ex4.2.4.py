import calendar

date = input("Enter a date: ")

day = calendar.weekday(int(date.split('/')[2]), int(date.split('/')[1]), int(date.split('/')[0]))
if(day == 0):
    print("Monday")
elif(day == 1):
    print("Tuesday")
elif(day == 2):
    print("Wednesday")
elif(day == 3):
    print("Thursday")
elif(day == 4):
    print("Friday")
elif(day == 5):
    print("Saturday")
else:
    print("Sunday")
