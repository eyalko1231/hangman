def longest(my_list):
    '''
    this function gets a list and returns the longest varible
    :param my_list:
    :return:
    '''
    return sorted(list(my_list),key=len)[-1]