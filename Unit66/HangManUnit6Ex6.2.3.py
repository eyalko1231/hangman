def format_list(my_list):
    '''
    this function gets a odd list of strings and returns string of the
    even places in the list with the last place with and
    :param my_list:
    :return:
    '''
    return ", ".join(my_list[::2])  + ", and " + my_list[-1]

print(format_list(['1','2','3','4','5']))


