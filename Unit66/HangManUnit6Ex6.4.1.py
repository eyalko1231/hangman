def check_valid_input(letter_guessed, old_letters_guessed):
    '''
       this function gets a letter and a letter list,
       and check if the letter given is valid and if it not in the
       letter list
       :param letter_guessed:
       :param old_letters_guessed:
       :return true if the letter is valid and false otherwise:
       '''
    return ((len(letter_guessed) < 2) and str(letter_guessed).isalpha()
            and str(letter_guessed).lower()
            not in old_letters_guessed)